# Homework

## Features

- Simple REST API with [Swagger specs](./docs/swagger.json):
    - Stuff resource CRUD.
    - Stuff resource search.
- Search is provided by Elastic + LogStash (LogStash periodically exports data from PostgreSQL to Elastic).
- Kubernetes deployment.
- Prometheus pods monitoring via Prometheus Operator (check [targets](http://localhost:9090/targets#pool-serviceMonitor/homework/api/0) in panel or check [logs](http://localhost:9090/graph?g0.expr=%7Bnamespace%3D%22homework%22%2C%20service%3D%22api%22%7D&g0.tab=1&g0.stacked=0&g0.range_input=1h) directly).

## TODO

- Setup Kubernetes Ingress.

## Demo environment

Demo environment can be launched by running following commands:

```bash
$ kubectl apply -f deployments/namespace.yml
# setup prometheus operator for proper pods monitoring
$ helm install prometheus-community/kube-prometheus-stack --namespace homework --generate-name
$ kubectl apply -f deployments/postgres.yml
$ kubectl apply -f deployments/elk.yml
$ kubectl apply -f deployments/api.yml
$ kubectl apply -f deployments/prom.yml
$ kubectl port-forward services/api -n homework 8080:8080
# or `kubectl port-forward -n homework prometheus-kube-prometheus-stack-1624-prometheus-0 9090` for Prometheus access
# or `kubectl port-forward -n homework service/kube-prometheus-stack-1624799512-grafana 3000:80` for Grafana access
```

For Graphana access use secrets from Prometheus Operator secret:

```bash
$ kubectl get secret -n homework kube-prometheus-stack-1624799512-grafana -o jsonpath="{.data.admin-user}" | base64 -d
$ kubectl get secret -n homework kube-prometheus-stack-1624799512-grafana -o jsonpath="{.data.admin-password}" | base64 -d
```

## Demo requests

```bash
$ http GET localhost:8080/api/v1/specs
HTTP/1.1 200 OK
Accept-Ranges: bytes
Connection: keep-alive
Content-Length: 7522
Content-Type: application/json
Date: Sun, 27 Jun 2021 15:47:54 GMT
ETag: "60d87f7f-1d62"
Last-Modified: Sun, 27 Jun 2021 13:39:11 GMT
Server: nginx/1.21.0

{
    "basePath": "/api/v1",
    "definitions": {
        "FindStuff": {
            "properties": {
                "description": {
                    "type": "string"
                }
            },
            "type": "object"
    ...
}


$ http POST localhost:8080/api/v1/stuff name='Amazing stuff #1' description='This stuff is on top'
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 72
Content-Type: application/json
Date: Sun, 27 Jun 2021 15:48:20 GMT
Server: nginx/1.21.0

{
    "description": "This stuff is on top",
    "id": 1,
    "name": "Amazing stuff #1"
}


$ http POST localhost:8080/api/v1/stuff name='Amazing stuff #2' description='This stuff is hard to find'
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 78
Content-Type: application/json
Date: Sun, 27 Jun 2021 15:48:41 GMT
Server: nginx/1.21.0

{
    "description": "This stuff is hard to find",
    "id": 2,
    "name": "Amazing stuff #2"
}


$ http GET localhost:8080/api/v1/stuff/2
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 78
Content-Type: application/json
Date: Sun, 27 Jun 2021 15:48:48 GMT
Server: nginx/1.21.0

{
    "description": "This stuff is hard to find",
    "id": 2,
    "name": "Amazing stuff #2"
}


$ http POST localhost:8080/api/v1/stuff/findByDescription description='hard'
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 54
Content-Type: application/json
Date: Sun, 27 Jun 2021 15:49:04 GMT
Server: nginx/1.21.0

[
    {
        "description": "This stuff is hard to find",
        "id": 2
    }
]


$ http GET 'localhost:8080/api/v1/stuff?page=1&perPage=1'
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 80
Content-Type: application/json
Date: Sun, 27 Jun 2021 15:49:50 GMT
Server: nginx/1.21.0

[
    {
        "description": "This stuff is hard to find",
        "id": 2,
        "name": "Amazing stuff #2"
    }
]
```