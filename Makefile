.PHONY: deps-get server-install db-create-migration db-upgrade db-downgrade

# install migrate separately to prevent its removal after 'go mod tidy' call
deps-get:
	go get -d -v ./...
	go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest

db-create-migration:
	migrate create -dir migrations -ext sql $(name)

db-upgrade:
	migrate -source file://./migrations -database '${db}' up

db-downgrade:
	migrate -source file://./migrations -database '${db}' down 1

install:
	go install -v ./cmd/server
