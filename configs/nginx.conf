worker_processes 1;

events {
    worker_connections 1024;
    use epoll;
    multi_accept on;
}

http {
    # basic params
    client_max_body_size 1M;

    # performance tuning
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;

    server {
        listen 8080 default_server;

        # return api/v1 specs
        location /api/v1/specs {
            default_type application/json;
            alias /etc/nginx/templates/api/v1/swagger.json;
        }

        # proxy to api/v1
        location /api/v1 {
            proxy_pass http://localhost:8081;
        }

        # proxy to metrics
        location /metrics {
            proxy_pass http://localhost:8081;
        }
    }
}