package search

import (
	"go.uber.org/fx"
	"homework/pkg/config"
	"homework/pkg/search/elastic"
	"homework/pkg/search/interfaces"
)

type Factory interface {
	Stuff() interfaces.Stuff
}

// InstantiateQueries instantiates multiple queries by calls to abstract factory
func InstantiateQueries(f Factory) interfaces.Stuff {
	return f.Stuff()
}

func NewFactoryFromConfig(c config.Config) Factory {
	switch c.Search.Driver {
	case "elastic":
		return elastic.New(c.Search.Dsn)
	default:
		panic(c.Repository.Driver)
	}
}

var Provider = fx.Provide(
	InstantiateQueries,
	NewFactoryFromConfig,
)
