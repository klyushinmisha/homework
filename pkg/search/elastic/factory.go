package elastic

import (
	"github.com/elastic/go-elasticsearch/v8"
	"homework/pkg/search/interfaces"
	"strings"
)

type ElasticFactory struct {
	client *elasticsearch.Client
}

func New(dsn string) *ElasticFactory {
	cfg := elasticsearch.Config{
		Addresses: strings.Split(dsn, ";"),
	}
	client, err := elasticsearch.NewClient(cfg)
	if err != nil {
		panic(err)
	}
	return &ElasticFactory{client}
}

func (f *ElasticFactory) Stuff() interfaces.Stuff {
	return &ElasticStuff{f.client}
}
