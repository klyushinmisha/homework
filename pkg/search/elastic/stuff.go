package elastic

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esapi"
	"homework/pkg/search/meta"
	"io"
)

type ElasticStuff struct {
	client *elasticsearch.Client
}

func (s *ElasticStuff) makeRequest(query interface{}) (*esapi.Response, error) {
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(query); err != nil {
		return nil, err
	}
	return s.client.Search(
		// TODO: handle deadlines
		s.client.Search.WithContext(context.Background()),
		s.client.Search.WithIndex("stuff"),
		s.client.Search.WithBody(&buf),
		s.client.Search.WithTrackTotalHits(true),
		s.client.Search.WithPretty(),
	)
}

type elasticStuffQueryResponse struct {
	Hits struct {
		Hits []struct {
			Source struct {
				meta.StuffQueryResult
			} `json:"_source"`
		} `json:"hits"`
	} `json:"hits"`
}

func extractStuffQueryRequestCollection(r io.Reader) ([]meta.StuffQueryResult, error) {
	payload := elasticStuffQueryResponse{}
	if err := json.NewDecoder(r).Decode(&payload); err != nil {
		return nil, err
	}
	stuffs := []meta.StuffQueryResult{}
	for _, hit := range payload.Hits.Hits {
		stuffs = append(stuffs, hit.Source.StuffQueryResult)
	}
	return stuffs, nil
}

type elasticErrorResponse struct {
	Error struct {
		Type   string `json:"type"`
		Reason string `json:"reason"`
	} `json:"error"`
}

func (r *elasticErrorResponse) String() string {
	return fmt.Sprintf(`Elastic query failed:
	type: %s
	reason: %s`,
		r.Error.Type,
		r.Error.Reason,
	)
}

func extractError(r io.Reader) error {
	payload := new(elasticErrorResponse)
	if err := json.NewDecoder(r).Decode(payload); err != nil {
		return err
	}
	return errors.New(payload.String())
}

func (s *ElasticStuff) Query(opts meta.StuffQueryOpts) ([]meta.StuffQueryResult, error) {
	query := map[string]interface{}{
		"query": map[string]interface{}{
			"match": map[string]interface{}{
				"description": opts.Description,
			},
		},
	}
	res, err := s.makeRequest(query)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	if res.IsError() {
		return nil, extractError(res.Body)
	}
	return extractStuffQueryRequestCollection(res.Body)
}
