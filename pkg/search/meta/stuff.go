package meta

type StuffQueryOpts struct {
	Description string
}

type StuffQueryResult struct {
	Id          int    `json:"id"`
	Description string `json:"description"`
}
