package interfaces

import (
	"homework/pkg/search/meta"
)

type Stuff interface {
	Query(opts meta.StuffQueryOpts) ([]meta.StuffQueryResult, error)
}
