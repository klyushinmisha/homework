package repositories

import (
	"go.uber.org/fx"
	"homework/pkg/config"
	"homework/pkg/repositories/interfaces"
	"homework/pkg/repositories/pq"
)

type Factory interface {
	Stuff() interfaces.Stuff
}

// InstantiateRepositories instantiates multiple repos by calls to abstract factory
func InstantiateRepositories(f Factory) interfaces.Stuff {
	return f.Stuff()
}

func NewFactoryFromConfig(c config.Config) Factory {
	switch c.Repository.Driver {
	case "postgres":
		return pq.New(c.Repository.Dsn)
	default:
		panic(c.Repository.Driver)
	}
}

var Provider = fx.Provide(
	InstantiateRepositories,
	NewFactoryFromConfig,
)
