package pq

import (
	"github.com/jmoiron/sqlx"
	"homework/pkg/models"
	"homework/pkg/repositories/meta"
)

type PostgresStuff struct {
	db *sqlx.DB
}

func (s *PostgresStuff) Get(opts meta.Options) ([]models.Stuff, error) {
	stuffs := []models.Stuff{}
	limit := opts.Pagination.PerPage
	offset := opts.Pagination.Page * opts.Pagination.PerPage
	err := s.db.Select(&stuffs, "SELECT * FROM stuff LIMIT $1 OFFSET $2", limit, offset)
	return stuffs, MetaError(err)
}

func (s *PostgresStuff) Create(stuff *models.Stuff) error {
	// adds id to stuff
	err := s.db.Get(
		stuff,
		"INSERT INTO stuff (name, description) VALUES ($1, $2) RETURNING *",
		stuff.Name,
		stuff.Description,
	)
	return MetaError(err)
}

// Put method provides a hacky way to put stuff into store;
// NOTE: Homework only. Mustn't be used in prod envs;
// User can set max id which leads to sequence overflow
func (s *PostgresStuff) Put(stuff models.Stuff) error {
	tx, err := s.db.Beginx()
	if err != nil {
		return MetaError(err)
	}
	defer tx.Rollback()
	// insert stuff with id conflict handling
	_, err = tx.NamedExec(`
		INSERT INTO stuff (id, name, description) VALUES (:id, :name, :description)
		ON CONFLICT (id) DO UPDATE SET
			name=excluded.name,
			description=excluded.description`,
		stuff,
	)
	if err != nil {
		return MetaError(err)
	}
	// NOTE: hack;
	// id sequence may be broken after insert;
	// fix it by making last id equal to max id from table
	_, err = tx.Exec(`
		SELECT setval(
			pg_get_serial_sequence(
				'stuff',
				'id'
			),
			COALESCE((SELECT MAX(id) FROM stuff), 1)
		);
	`)
	if err != nil {
		return MetaError(err)
	}
	tx.Commit()
	return MetaError(err)
}

func (s *PostgresStuff) GetById(id int) (models.Stuff, error) {
	stuff := new(models.Stuff)
	err := s.db.Get(stuff, "SELECT * FROM stuff WHERE id=$1", id)
	return *stuff, MetaError(err)
}

func (s *PostgresStuff) DeleteById(id int) error {
	_, err := s.db.Exec("DELETE FROM stuff WHERE id=$1", id)
	return MetaError(err)
}
