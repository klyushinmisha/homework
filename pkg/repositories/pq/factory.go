package pq

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"homework/pkg/repositories/interfaces"
)

func NewPostgresDBConn(dsn string) *sqlx.DB {
	// TODO: use ConnectContext for deadlining
	db, err := sqlx.Connect("postgres", dsn)
	if err != nil {
		panic(err)
	}
	return db
}

type PostgresFactory struct {
	db *sqlx.DB
}

func New(dsn string) *PostgresFactory {
	return &PostgresFactory{db: NewPostgresDBConn(dsn)}
}

func (f *PostgresFactory) Stuff() interfaces.Stuff {
	return &PostgresStuff{f.db}
}
