package pq

import (
	"database/sql"
	"homework/pkg/repositories/meta"
)

// MetaError creates meta error from driver-specific one
func MetaError(err error) error {
	switch err {
	case sql.ErrConnDone:
		return meta.ErrConnDone
	case sql.ErrNoRows:
		return meta.ErrNotFound
	case sql.ErrTxDone:
		return meta.ErrTxDone
	default:
		return err
	}
}
