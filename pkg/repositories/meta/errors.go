package meta

import (
	"errors"
)

var (
	ErrNotFound = errors.New("items not found in repository")
	ErrConnDone = errors.New("connection closed")
	ErrTxDone   = errors.New("transaction has already been finished")
)
