package meta

type Pagination struct {
	Page    int
	PerPage int
}

type Options struct {
	Pagination
}

type OptionsBuilder struct {
	Pagination
}

func NewOptionsBuilder() *OptionsBuilder {
	b := new(OptionsBuilder)
	b.Pagination.Page = 0
	b.Pagination.PerPage = 10
	return b
}

func (b *OptionsBuilder) SetPage(page int) *OptionsBuilder {
	b.Pagination.Page = page
	return b
}

func (b *OptionsBuilder) SetPerPage(perPage int) *OptionsBuilder {
	b.Pagination.PerPage = perPage
	return b
}

func (b *OptionsBuilder) Build() Options {
	return Options{
		Pagination: b.Pagination,
	}
}
