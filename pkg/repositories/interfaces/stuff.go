package interfaces

import (
	"homework/pkg/models"
	"homework/pkg/repositories/meta"
)

type Stuff interface {
	Get(opts meta.Options) ([]models.Stuff, error)
	Create(stuff *models.Stuff) error
	Put(stuff models.Stuff) error
	GetById(id int) (models.Stuff, error)
	DeleteById(id int) error
}
