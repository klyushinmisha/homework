package params

import (
	"errors"
	"net/url"
	"strconv"
)

type QueryArgsExtractor struct {
	query url.Values
}

func NewQueryArgsExtractor(query url.Values) *QueryArgsExtractor {
	e := new(QueryArgsExtractor)
	e.query = query
	return e
}

var ErrInvalidQueryArgType = errors.New("invalid query arg type")

func (e *QueryArgsExtractor) Exists(key string) bool {
	_, exists := e.query[key]
	return exists
}

func (e *QueryArgsExtractor) GetInt(key string, validate func(int) bool) (int, error) {
	value, err := strconv.Atoi(e.query.Get(key))
	if err != nil {
		return value, err
	}
	if !validate(value) {
		return value, ErrInvalidQueryArgType
	}
	return value, nil
}

func (e *QueryArgsExtractor) GetString(key string, validate func(string) bool) (string, error) {
	value := e.query.Get(key)
	if !validate(value) {
		return value, ErrInvalidQueryArgType
	}
	return value, nil
}
