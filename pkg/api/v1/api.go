package v1

import (
	"github.com/gorilla/mux"
	"homework/pkg/api/v1/stuff"
	"homework/pkg/middlewares"
)

type Api struct {
	stuff *stuff.Controller
}

func New(stuff *stuff.Controller) *Api {
	return &Api{stuff}
}

func (a *Api) Bind(apiRouter *mux.Router) {
	apiRouter.HandleFunc("/stuff", a.stuff.Get).Methods("GET")
	apiRouter.Handle(
		"/stuff",
		middlewares.BuilderFunc(a.stuff.Post).
			Use(middlewares.ContentType("application/json")).
			Build(),
	).Methods("POST")
	apiRouter.HandleFunc("/stuff/{id:[0-9]+}", a.stuff.GetById).Methods("GET")
	apiRouter.Handle(
		"/stuff/{id:[0-9]+}",
		middlewares.BuilderFunc(a.stuff.PutById).
			Use(middlewares.ContentType("application/json")).
			Build(),
	).Methods("PUT")
	apiRouter.HandleFunc("/stuff/{id:[0-9]+}", a.stuff.DeleteById).Methods("DELETE")
	apiRouter.Handle(
		"/stuff/findByDescription",
		middlewares.BuilderFunc(a.stuff.FindByDescription).
			Use(middlewares.ContentType("application/json")).
			Build(),
	).Methods("POST")
}

var Providers = []interface{}{
	New,
	stuff.New,
}
