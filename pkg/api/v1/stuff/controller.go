package stuff

import (
	"github.com/gorilla/mux"
	"homework/pkg/api/params"
	"homework/pkg/api/validator"
	"homework/pkg/logger"
	"homework/pkg/models"
	repositories "homework/pkg/repositories/interfaces"
	repoMeta "homework/pkg/repositories/meta"
	queries "homework/pkg/search/interfaces"
	searchMeta "homework/pkg/search/meta"
	"homework/pkg/transfer"
	"net/http"
	"strconv"
)

type Controller struct {
	validator  validator.Interface
	logger     logger.Interface
	stuffRepo  repositories.Stuff
	stuffQuery queries.Stuff
}

func New(
	logger logger.Interface,
	stuffRepo repositories.Stuff,
	stuffQuery queries.Stuff,
	validator validator.Interface,
) *Controller {
	return &Controller{validator, logger, stuffRepo, stuffQuery}
}

func (c *Controller) Get(w http.ResponseWriter, r *http.Request) {
	ext := params.NewQueryArgsExtractor(r.URL.Query())
	optsBuilder := repoMeta.NewOptionsBuilder()
	if ext.Exists("page") {
		if page, err := ext.GetInt("page", func(page int) bool { return page >= 0 }); err == nil {
			optsBuilder.SetPage(page)
		} else {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	}
	if ext.Exists("perPage") {
		if perPage, err := ext.GetInt("perPage", func(perPage int) bool { return perPage > 0 }); err == nil {
			optsBuilder.SetPerPage(perPage)
		} else {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	}
	stuffs, err := c.stuffRepo.Get(optsBuilder.Build())
	if err != nil {
		c.logger.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if err := transfer.NewEncoder(w, w.Header().Get("Content-Type")).Encode(stuffs); err != nil {
		c.logger.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

type NewStuff struct {
	Name        string `json:"name" validate:"required,min=1,max=32"`
	Description string `json:"description" validate:"max=128"`
}

func (c *Controller) Post(w http.ResponseWriter, r *http.Request) {
	payload := new(NewStuff)
	if err := transfer.NewDecoder(r.Body, r.Header.Get("Content-Type")).Decode(payload); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if err := c.validator.Struct(payload); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	stuff := models.Stuff{
		Name:        payload.Name,
		Description: payload.Description,
	}
	if err := c.stuffRepo.Create(&stuff); err != nil {
		c.logger.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if err := transfer.NewEncoder(w, w.Header().Get("Content-Type")).Encode(stuff); err != nil {
		c.logger.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (c *Controller) GetById(w http.ResponseWriter, r *http.Request) {
	// ignore error: router already checks int
	id, _ := strconv.Atoi(mux.Vars(r)["id"])
	stuff, err := c.stuffRepo.GetById(id)
	if err == repoMeta.ErrNotFound {
		w.WriteHeader(http.StatusNotFound)
		return
	} else if err != nil {
		c.logger.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if err := transfer.NewEncoder(w, w.Header().Get("Content-Type")).Encode(stuff); err != nil {
		c.logger.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (c *Controller) PutById(w http.ResponseWriter, r *http.Request) {
	// ignore error: router already checks int
	id, _ := strconv.Atoi(mux.Vars(r)["id"])
	payload := new(NewStuff)
	if err := transfer.NewDecoder(r.Body, r.Header.Get("Content-Type")).Decode(payload); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if err := c.validator.Struct(payload); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	stuff := models.Stuff{
		Id:          id,
		Name:        payload.Name,
		Description: payload.Description,
	}
	if err := c.stuffRepo.Put(stuff); err != nil {
		c.logger.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
}

func (c *Controller) DeleteById(w http.ResponseWriter, r *http.Request) {
	// ignore error: router already checks int
	id, _ := strconv.Atoi(mux.Vars(r)["id"])
	if err := c.stuffRepo.DeleteById(id); err != nil && err != repoMeta.ErrNotFound {
		c.logger.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
}

type FindStuff struct {
	Description string `json:"description" validate:"max=128"`
}

func (c *Controller) FindByDescription(w http.ResponseWriter, r *http.Request) {
	payload := new(FindStuff)
	if err := transfer.NewDecoder(r.Body, r.Header.Get("Content-Type")).Decode(payload); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if err := c.validator.Struct(payload); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	opts := searchMeta.StuffQueryOpts{Description: payload.Description}
	stuffs, err := c.stuffQuery.Query(opts)
	// TODO: reduce collection retrieval copy-paste
	if err != nil {
		c.logger.Error(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if err := transfer.NewEncoder(w, w.Header().Get("Content-Type")).Encode(stuffs); err != nil {
		c.logger.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
