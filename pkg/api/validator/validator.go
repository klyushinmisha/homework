package validator

import (
	"github.com/go-playground/validator/v10"
)

type Interface interface {
	Struct(interface{}) error
}

func New() Interface {
	// use a single instance of validator to cache structs info
	return validator.New()
}
