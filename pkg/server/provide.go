package server

import (
	"fmt"
	"go.uber.org/fx"
	"homework/pkg/config"
	"net/http"
)

func New(handler http.Handler, c config.Config) Server {
	return &http.Server{
		Addr:    fmt.Sprintf(":%d", c.Server.Port),
		Handler: handler,
	}
}

var Provider = fx.Provide(New)
