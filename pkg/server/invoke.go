package server

import (
	"context"
	"go.uber.org/fx"
)

func AttachServerToLifecycle(server Server, lc fx.Lifecycle) {
	lc.Append(fx.Hook{
		OnStart: func(context.Context) error {
			go func() {
				server.ListenAndServe()
			}()
			return nil
		},
		OnStop: func(ctx context.Context) error {
			return server.Shutdown(ctx)
		},
	})
}

var Invoker = fx.Invoke(AttachServerToLifecycle)
