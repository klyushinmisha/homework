package transfer

import (
	"encoding/json"
	"io"
)

// Decoder is a Content-Type-specific request payload decoder;
// data can be represented by concrete trasfer format (SOAP, JSON, XML, GraphQL)
type Decoder interface {
	Decode(interface{}) error
}

// Encoder is a Content-Type-specific payload encoder;
// data can be represented by concrete trasfer format (SOAP, JSON, XML, GraphQL)
type Encoder interface {
	Encode(interface{}) error
}

func NewDecoder(r io.Reader, contentType string) Decoder {
	switch contentType {
	case "application/json":
		return json.NewDecoder(r)
	default:
		return nil
	}
}

func NewEncoder(w io.Writer, contentType string) Encoder {
	switch contentType {
	case "application/json":
		return json.NewEncoder(w)
	default:
		return nil
	}
}
