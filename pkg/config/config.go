package config

import (
	"github.com/kelseyhightower/envconfig"
	"go.uber.org/fx"
)

type Server struct {
	Port int `envconfig:"server_port" default:"8080"`
}

type Repository struct {
	Driver string `envconfig:"repository_driver" default:"postgres"`
	Dsn    string `envconfig:"repository_dsn"`
}

type Search struct {
	Driver string `envconfig:"search_driver" default:"elastic"`
	Dsn    string `envconfig:"search_dsn"`
}

type Config struct {
	Server
	Repository
	Search
}

func FromEnv() Config {
	c := Config{}
	err := envconfig.Process("app", &c)
	if err != nil {
		panic(err)
	}
	return c
}

var Provider = fx.Provide(FromEnv)
