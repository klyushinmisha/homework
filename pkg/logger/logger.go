package logger

import (
	"bytes"
	"fmt"
	"log"
	"os"
)

const (
	INFO  = "INFO"
	WARN  = "WARN"
	ERROR = "ERROR"
)

type Interface interface {
	Info(args ...interface{})
	Warn(args ...interface{})
	Error(args ...interface{})
	Fatal(args ...interface{})
	Panic(args ...interface{})
}

type DefaultLogWrapper struct {
	*log.Logger
}

func NewLogger() Interface {
	logger := log.New(os.Stderr, "[app] ", log.Ldate|log.Ltime|log.Lmsgprefix)
	return &DefaultLogWrapper{logger}
}

func buildMsg(level string, args []interface{}) string {
	buffer := bytes.Buffer{}
	buffer.WriteString(level)
	buffer.WriteString(" ")
	for _, arg := range args {
		buffer.WriteString(fmt.Sprintf("%v", arg))
		buffer.WriteString(" ")
	}
	return buffer.String()
}

func (w *DefaultLogWrapper) Info(args ...interface{}) {
	w.Print(buildMsg(INFO, args))
}

func (w *DefaultLogWrapper) Warn(args ...interface{}) {
	w.Print(buildMsg(WARN, args))
}

func (w *DefaultLogWrapper) Error(args ...interface{}) {
	w.Print(buildMsg(ERROR, args))
}

func (w *DefaultLogWrapper) Fatal(args ...interface{}) {
	w.Fatal(buildMsg(ERROR, args))
}

func (w *DefaultLogWrapper) Panic(args ...interface{}) {
	w.Panic(buildMsg(ERROR, args))
}
