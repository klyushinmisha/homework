package logger

import (
	"go.uber.org/fx"
)

var Provider = fx.Provide(NewLogger)
