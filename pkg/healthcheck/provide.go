package healthcheck

import (
	"net/http"
)

func DumbOkHandler(w http.ResponseWriter, r *http.Request) {
	// do nothing
}

func Handler() http.Handler {
	return http.Handler(http.HandlerFunc(DumbOkHandler))
}
