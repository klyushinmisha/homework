package middlewares

import (
	"net/http"
)

type contentTypeMiddleware struct {
	types []string
}

func ContentType(types ...string) *contentTypeMiddleware {
	m := new(contentTypeMiddleware)
	m.types = types
	return m
}

func (m *contentTypeMiddleware) check(contentType string) bool {
	for _, ct := range m.types {
		if ct == contentType {
			return true
		}
	}
	return false
}

type contentTypeMiddlewareHandler struct {
	m *contentTypeMiddleware
	h http.Handler
}

func (h contentTypeMiddlewareHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if h.m.check(r.Header.Get("Content-Type")) {
		h.h.ServeHTTP(w, r)
	} else {
		w.WriteHeader(http.StatusUnsupportedMediaType)
	}
}

func (m *contentTypeMiddleware) Bind(h http.Handler) http.Handler {
	return contentTypeMiddlewareHandler{m, h}
}
