package middlewares

import (
	"net/http"
)

type Middleware interface {
	Bind(h http.Handler) http.Handler
}

type builder struct {
	h           http.Handler
	middlewares []Middleware
}

func Builder(h http.Handler) *builder {
	b := new(builder)
	b.h = h
	return b
}

func BuilderFunc(hf http.HandlerFunc) *builder {
	return Builder(http.Handler(hf))
}

func (b *builder) Use(m Middleware) *builder {
	b.middlewares = append([]Middleware{m}, b.middlewares...)
	return b
}

func (b *builder) Build() http.Handler {
	h := b.h
	for _, m := range b.middlewares {
		h = m.Bind(h)
	}
	return h
}
