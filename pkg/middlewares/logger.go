package middlewares

import (
	"homework/pkg/logger"
	"net/http"
)

// loggerResponseWriterDecorator decorates provided http.ResponseWriter
type loggerResponseWriterDecorator struct {
	http.ResponseWriter
	statusCode int
}

func (w *loggerResponseWriterDecorator) WriteHeader(code int) {
	w.ResponseWriter.WriteHeader(code)
	w.statusCode = code
}

type loggerMiddleware struct {
	logger logger.Interface
}

func Logger(logger logger.Interface) *loggerMiddleware {
	m := new(loggerMiddleware)
	m.logger = logger
	return m
}

func (m *loggerMiddleware) log(lw *loggerResponseWriterDecorator, r *http.Request) {
	m.logger.Info(r.Method, r.URL.Path, r.RemoteAddr, r.UserAgent(), lw.statusCode)
}

type loggerMiddlewareHandler struct {
	m *loggerMiddleware
	h http.Handler
}

func (h loggerMiddlewareHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	lw := &loggerResponseWriterDecorator{w, http.StatusOK}
	h.h.ServeHTTP(lw, r)
	h.m.log(lw, r)
}

func (m *loggerMiddleware) Bind(h http.Handler) http.Handler {
	return loggerMiddlewareHandler{m, h}
}
