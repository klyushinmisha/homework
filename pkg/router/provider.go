package router

import (
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/fx"
	"homework/pkg/api/v1"
	"homework/pkg/healthcheck"
	"homework/pkg/logger"
	"homework/pkg/middlewares"
	"net/http"
)

func New(apiV1 *v1.Api, logger logger.Interface) http.Handler {
	r := mux.NewRouter()
	apiV1.Bind(r.PathPrefix("/api/v1").Subrouter())
	// NOTE: use default metrics for app simplicity
	// TODO: add DB.Stats() from database/sql
	r.Handle("/metrics", promhttp.Handler()).Methods("GET")
	r.Handle("/health", healthcheck.Handler()).Methods("GET")
	return middlewares.Builder(r).
		Use(middlewares.Logger(logger)).
		Build()
}

var Provider = fx.Provide(New)
