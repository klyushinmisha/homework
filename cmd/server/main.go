package main

import (
	"go.uber.org/fx"
	"homework/pkg/api"
	"homework/pkg/config"
	"homework/pkg/logger"
	"homework/pkg/repositories"
	"homework/pkg/router"
	"homework/pkg/search"
	"homework/pkg/server"
)

func main() {
	app := fx.New(
		api.Provider,
		config.Provider,
		logger.Provider,
		repositories.Provider,
		router.Provider,
		search.Provider,
		server.Provider,
		server.Invoker,
	)
	if app.Err() != nil {
		panic(app.Err())
	}
	app.Run()
}
